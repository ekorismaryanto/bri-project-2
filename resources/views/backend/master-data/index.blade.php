@extends('backend.layout')

@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<h4>Tambah Kategori</h4>
			<hr>
			{{ Form::open(array('url' => route('admin.master-data.store-kategori'), 'method' => 'post', 'class' => 'form-horizontal form-label-left')) }}
					<div class="form-group">
						<label class="col-md-1 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
						</label>
						<div class="col-md-7 col-sm-6 col-xs-12">
							<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="nama_kategori">
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<button class="btn btn-primary" type="reset">Reset</button>
							<button type="submit" class="btn btn-success">Submit</button>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Nama Kategori</th>
							<th>Aksi</th>
						</tr>
					</thead>
					@foreach($kategori as $row)
						<tr>
							<td>{{ $row->nama_kategori }}</td>
							<td></td>
						</tr>
					@endforeach

					<tbody>
					</tbody>
				</table>
			</div>
		</div>

		<div class="x_panel">
			<h4>Tambah Sub Kategori</h4>
			<hr>
			{{ Form::open(array('url' => route('admin.master-data.store-sub-kategori'), 'method' => 'post', 'class' => 'form-horizontal form-label-left')) }}
					<div class="form-group">
						<label class="col-md-1 col-sm-3 col-xs-12" for="first-name">kateori <span class="required">*</span>
						</label>
						<div class="col-md-7 col-sm-6 col-xs-12">
							<select class="sub-kategori form-control" name="id_kategori">

							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-1 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
						</label>
						<div class="col-md-7 col-sm-6 col-xs-12">
							<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="nama_sub_kategori">
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<button class="btn btn-primary" type="reset">Reset</button>
							<button type="submit" class="btn btn-success">Submit</button>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Nama Kategori</th>
							<th>Nama Sub Kategori</th>
							<th>Aksi</th>
						</tr>
					</thead>
					@foreach($subKategori as $row)
						<tr>
							<td>{{ $row->kategori->nama_kategori }}</td>
							<td>{{ $row->nama_sub_kategori }}</td>
							<td></td>
						</tr>
					@endforeach

					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script type="text/javascript">
	      $('.sub-kategori').select2({
	        placeholder: 'Select an item',
	        ajax: {
	          url: '{{ route('admin.master-data.ajax-category') }}',
	          dataType: 'json',
	          delay: 10,
	          processResults: function (data) {
	            return {
					results: data.results
				};
	          },
	          cache: true
	        }
	      });
</script>

@endsection