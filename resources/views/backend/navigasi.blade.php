<div class="profile clearfix">
  <div class="profile_pic">
    <img src="images/img.jpg" alt="..." class="img-circle profile_img">
  </div>
  <div class="profile_info">
    <span>Welcome,</span>
    <h2>John Doe</h2>
  </div>
</div>

<br />

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ route('admin.dashboard') }}">list Data</a></li>
          <li><a href="{{ route('admin.data.create') }}">Tambah Data</a></li>
        </ul>
      </li>

       <li><a href="{{ route('admin.master-data.') }}"><i class="fa fa-cog"></i> Master Data </span></a></li>
      
      <li><a><i class="fa fa-users"></i> User <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ route('admin.user.list') }}">Data List</a></li>
          <li><a href="{{ route('admin.user.create') }}">Tambah</a></li>
        </ul>
      </li>

      <li><a><i class="fa fa-cogs"></i> Permission <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ route('admin.role.list') }}">Data List</a></li>
          <li><a href="{{ route('admin.role.create') }}">Tambah</a></li>
        </ul>
      </li>

    </ul>
  </div>

</div>
<!-- /sidebar menu -->