@extends('backend.layout')

@section('content')

<style type="text/css">
	.t-center{
		text-align: center;
	}
</style>


<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data List</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
					<div class="row">
						<div class="col-sm-6">
							<div class="dataTables_length" id="datatable_length">
								<label>Show 
									<select name="datatable_length" aria-controls="datatable" class="form-control input-sm">
										<option value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select> 
								entries</label>
							</div>
						</div>
						<div class="col-sm-6">
							<div id="datatable_filter" class="dataTables_filter">
								<label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable"></label></div></div>
							</div>
							<div class="row">
								<div class="col-sm-12" style="overflow-x:auto;">
									<table class="table table-striped table-bordered" role="grid" aria-describedby="datatable_info">
										<thead>
											<tr>
												<td rowspan="2" class="t-center">No</td>
												<td rowspan="2" class="t-center">PID</td>
												@foreach($subKategoris->groupby('id_kategori') as $key => $kategori)
													<td class="t-center" colspan="{{ $kategori[$key]->count_data }}">{{ $kategori[$key]->kategori->nama_kategori }}</td>
												@endforeach
												<td class="t-center" rowspan="2">Keterangan</td>
											</tr>
											<tr>
												@foreach($subKategoris as $key => $kategori)
													<td>{{ $kategori->nama_sub_kategori }}</td>
												@endforeach

											</tr>
										</thead>
										<tbody>
											@forelse($checklist as $row)
											<tr>
												<td></td>
												<td>{{ $row->tid_id }}</td>
												@foreach($row->checklist as $check)
												<td>{{ $check->status == 1 ? 'OK' : 'NOT OK'}}</td>
												@endforeach
												<td>{{ $row->keterangan }}</td>											
											</tr>
											@empty
											<tr>
												<td colspan="{{ count($subKategoris) +3 }}">Data Kosong</td>
											</tr>

											@endforelse
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-5">
								</div>
								<div class="col-sm-7">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		@endsection