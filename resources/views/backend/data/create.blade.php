@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Create Data- <small>{{ date('d-m-Y') }}</small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        
        {{ Form::open(array('url' => route('admin.data.store'), 'method' => 'post', 'class' => 'form-horizontal form-label-left')) }}

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">PID</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" class="form-control" placeholder="PID" value="{{ $tid }}" name="tid" readonly="">
            </div>
          </div>
  
          @foreach($subKategoris as $row)
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $row->nama_sub_kategori }} : </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="">
                <label>
                 Not Ok <input type="checkbox" class="js-switch" name="id_sub_kategori[]" value="{{ $row->id }} " /> Ok
                </label>
              </div>
            </div>
          </div>
          @endforeach

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" class="form-control" placeholder="keterangan" value="" name="keterangan">
            </div>
          </div>


          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-primary">Cancel</button>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>

        {{ Form::close() }}
        
      </div>
    </div>
  </div>
</div>
@endsection