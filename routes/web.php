<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'login', 'middleware' => 'guest', 'uses' => '\App\Http\Controllers\HomeController@index']);
Route::post('/do-login', ['as' => 'do-login', 'uses' => '\App\Http\Controllers\HomeController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => '\App\Http\Controllers\HomeController@logout']);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function () {

    Route::get('/', ['as' => 'dashboard', 'uses' => '\App\Http\Controllers\backend\DashboardController@index']);

    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/list', ['as' => 'list', 'uses' => '\App\Http\Controllers\backend\UserController@index']);
        Route::get('/create', ['as' => 'create', 'uses' => '\App\Http\Controllers\backend\UserController@create']);
        Route::post('/store', ['as' => 'store', 'uses' => '\App\Http\Controllers\backend\UserController@store']);
    });

    Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
        Route::get('/list', ['as' => 'list', 'uses' => '\App\Http\Controllers\backend\RoleController@index']);
        Route::get('/create', ['as' => 'create', 'uses' => '\App\Http\Controllers\backend\RoleController@create']);
    });

    Route::group(['prefix' => 'master-data', 'as' => 'master-data.'], function () {
        Route::get('/', ['uses' => '\App\Http\Controllers\backend\MasterDataController@indexKategori']);
        Route::get('/ajax-category', ['as' => 'ajax-category' , 'uses' => '\App\Http\Controllers\backend\MasterDataController@ajaxKategori']);
        Route::post('/store-kategori', ['as' => 'store-kategori', 'uses' => '\App\Http\Controllers\backend\MasterDataController@storeKategori']);
        Route::post('/store-sub-kategori', ['as' => 'store-sub-kategori', 'uses' => '\App\Http\Controllers\backend\MasterDataController@storeSubKategori']);

    });

      Route::group(['prefix' => 'data', 'as' => 'data.'], function () {
        Route::get('/create', ['as' => 'create', 'uses' => '\App\Http\Controllers\backend\DashboardController@create']);
        Route::post('/store', ['as' => 'store', 'uses' => '\App\Http\Controllers\backend\DashboardController@store']);

    });

});