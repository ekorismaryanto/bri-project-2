<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';

    protected $fillable = ['nama_kategori'];

    protected $table = 'kategori';

    public function sub_kategori()
    {
        return $this->belongsToMany(\App\Model\SubKategori::class);
    }

}