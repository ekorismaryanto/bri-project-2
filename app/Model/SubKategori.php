<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';

    protected $fillable = ['id_kategori','nama_sub_kategori'];

    protected $table = 'sub_kategori';

    public function kategori()
    {
        return $this->belongsTo('\App\Model\Kategori','id_kategori','id');
    }

}