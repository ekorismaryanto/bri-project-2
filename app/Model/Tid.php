<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tid extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tid_id','id_user','keterangan'];

    protected $table = 'data_tid';

    public function checklist()
    {
        return $this->hasMany('\App\Model\DataChecklist','id_tid','id');
    }

}