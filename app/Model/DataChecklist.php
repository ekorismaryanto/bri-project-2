<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataChecklist extends Model
{

    protected $fillable = ['id_tid','status','kode_tid','id_sub_kategori'];

    protected $table = 'data_checklist';

    public function sub_kategori()
    {
        return $this->belongsToMany(\App\Model\SubKategori::class);
    }

}