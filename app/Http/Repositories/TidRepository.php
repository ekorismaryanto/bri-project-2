<?php

namespace App\Http\Repositories;

use App\Model\Tid;

class TidRepository 
{

    public function getIdOrdDesc()
    {
        return Tid::select('id')->orderBy('id', 'desc')->firstOrFail();       
    }

    public function getData($data = '')
    {
        return Tid::with(['checklist'])->get();       
    }

    public function store($data)
    {
        return Tid::create($data);       
    }


}