<?php

namespace App\Http\Repositories;

use App\Model\SubKategori;

class SubKategoryRepository 
{

    public function getData()
    {
        return SubKategori::with('kategori')->selectRaw('
        	*,
        	 ( select count(k.id) 
                from sub_kategori as k
                join  kategori on kategori.id =  k.id_kategori
                where k.id_kategori = sub_kategori.id_kategori GROUP BY k.id_kategori 
            ) as count_data
        	')->get();       
    }

    public function store($data)
    {
        return SubKategori::create($data);    
    }
}