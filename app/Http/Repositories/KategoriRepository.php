<?php

namespace App\Http\Repositories;

use App\Model\Kategori;

class KategoriRepository 
{

    public function getData($data = '')
    {
        $query = Kategori::orderBy('id');

        if (!empty($data['q'])) {
        	$query->where('nama_kategori', 'like', '%'.$data['q'].'%');
        }       

        return $query->get();
    }

    public function store($data)
    {
        return Kategori::create($data);       
    }
}