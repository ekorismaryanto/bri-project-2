<?php

namespace App\Http\Repositories;

use App\Role;
use App\User;

class UserRepository 
{

    public function getData()
    {
        return User::get();       
    }

    public function getRole()
    {
        return Role::get();       
    }

}