<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Repositories\RoleRepository;
use App\Http\Repositories\UserRepository;
use App\Role;
use App\User;

class UserController extends Controller
{

	protected $user;

	public function __construct(UserRepository $user)
	{
		$this->user = $user;
	}

    public function index()
    {
    	$users = $this->user->getData();
        return view('backend.user.index', compact('users'));
    }

    public function create()
    {
        $roles = $this->user->getRole();
        return view('backend.user.create', compact('roles'));
    }

    public function store(Request $request)
    {

        $user = new User();

        $data = $request->all();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->save();
        $user->roles()->attach($data['roles']);
        return redirect(route('admin.user'));
    }
}