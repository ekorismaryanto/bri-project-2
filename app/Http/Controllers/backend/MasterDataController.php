<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Repositories\KategoriRepository;
use App\Http\Repositories\SubKategoryRepository;
use Illuminate\Http\Request;

class MasterDataController extends Controller
{

	protected $kategori;
	protected $subKategori;

	public function __construct(KategoriRepository $kategori, SubKategoryRepository $subKategori)
	{
		$this->kategori = $kategori;
		$this->subKategori = $subKategori;
	}


    public function indexKategori()
    {
    	$kategori = $this->kategori->getData();
        $subKategori = $this->subKategori->getData();

        return view('backend.master-data.index', compact('kategori','subKategori'));
    }

    public function storeKategori(Request $request)
    {
        $data = $request->all();
        $this->kategori->store($data);
        return redirect(route('admin.master-data.'));
    }

    public function storeSubKategori(Request $request)
    {
        $data = $request->all();
        $this->subKategori->store($data);
        return redirect(route('admin.master-data.'));
    }


    public function ajaxKategori(Request $request)
    {
        $search = $request->all() ?? [];
        $kategori =  $this->kategori->getData($search);
        foreach ($kategori as $key => $row) {
            $data[] = [
                'id' => $row->id,
                'text' => $row->nama_kategori
            ];
        }
        $datas = ['results' => $data];
        return response()->json($datas);
    }


}