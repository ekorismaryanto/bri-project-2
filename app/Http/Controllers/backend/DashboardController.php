<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Repositories\KategoriRepository;
use App\Http\Repositories\TidRepository;
use App\Http\Repositories\SubKategoryRepository;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{

	protected $kategori;

	protected $subKategori;

    protected $tid;

	public function __construct(KategoriRepository $kategori, SubKategoryRepository $subKategori, TidRepository $tid)
	{
		$this->kategori = $kategori;
		$this->subKategori = $subKategori;
        $this->tid = $tid;

	}


    public function index(Request $request)
    {
        $data = $request->all();
    	$subKategoris = $this->subKategori->getData();
        $checklist = $this->tid->getData($data);
        return view('backend.data.index', compact('subKategoris','checklist'));
    }


    public function create()
    {

        $tid = $this->createTID();
    	$subKategoris = $this->subKategori->getData();
        return view('backend.data.create', compact('subKategoris','tid'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $dataTid = array('tid_id' => $data['tid'], 'id_user' => Auth::user()->id, 'keterangan' => $data['keterangan']);
        $tid =  $this->tid->store($dataTid);
        $subKategoris = $this->subKategori->getData();
        foreach($subKategoris as $i => $row) { 
            $a = array_search($row->id, $data['id_sub_kategori'] ?? []);
            if ( ( $data['id_sub_kategori'][$a] ?? '' ) == $row->id ) {
                $data_checklist[] = array(
                        'id_sub_kategori' =>  $row->id ,
                        'status' => 1, 
                        'id_tid' => $tid->id,
                        'kode_tid' => $data['tid']
                       
                    ); 
            }else{
                $data_checklist[] = array(
                        'id_sub_kategori' =>  $row->id ,
                        'status' => 0, 
                        'id_tid' => $tid->id,
                        'kode_tid' => $data['tid'],
                    );
            }
        }

        $tid->checklist()->createMany($data_checklist);
        return redirect(route('admin.dashboard'));
    }


    public function createTID()
    {
         $userId = Auth::user()->id;
         return $tid = 'PID-'.$userId.'-'.date('dmis');
    }
}